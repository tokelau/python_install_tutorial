1.	Запускаем подходящий установщик
2.	Ставим галочку `Add python.exe to PATH` (чтобы скрипты на python можно было запускать с помощью команды python, а не через полный путь, например, `C:/Programs/Python/Python38/python.exe`) и переходим к кастомизированной установке. Если вместо нее выбрать _Install Now_, то будут установлены все дополнительные приложения, включая документацию и встроенную среду разработки
![1](https://gitlab.com/tokelau/python_install_tutorial/-/raw/main/images/1.png)

3.	На следующем шаге оставляем только pip 
![2](https://gitlab.com/tokelau/python_install_tutorial/-/raw/main/images/2.png)

4.	Выбираем нужные галочки и выбираем пусть установки 
![3](https://gitlab.com/tokelau/python_install_tutorial/-/raw/main/images/3.png)

5.	Открываем командную строку, вводим python, должен появиться подобный вывод 
![4](https://gitlab.com/tokelau/python_install_tutorial/-/raw/main/images/4.png)

6.	Определяем путь, где размещен файл **requirements.txt** и запускаем команду `pip install -r`. Например, `pip install -r C:\Users\atl\Downloads\requirements.txt` Вывод должен получиться примерно такой 
![5](https://gitlab.com/tokelau/python_install_tutorial/-/raw/main/images/5.png)

Готово!
